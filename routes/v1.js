const joi = require('joi');

let campaignController = require('../controllers/campaign.controller');
let voteController = require('../controllers/vote.controller');

module.exports = [
    {
        method: ["GET"], 
        path: '/campaign/{id}', 
        config: {
            tags: ['api'],
            auth: false , 
            validate:{
                params:{
                    id: joi.number().required()
                }
                //payload,path params
            },
        },
        handler: async function(request, h) {
            let response = campaignController.get(request.params,{});
            return response;
        }
    },
    {
        method: ["GET"], 
        path: '/campaign-list/', 
        config: {
            tags: ['api'],
            auth: false
        },
        handler: async function(request, h) {
            let response = campaignController.get_list({},{});
            return response;
        }
    },
    {
        method: ["POST"], 
        path: '/vote/create/', 
        config: {
            tags: ['api'],
            auth: false , 
            validate:{
                payload:{
                    hkid: joi.string().regex(/^[A-MP-Za-mp-z]{1,2}[0-9]{6}[0-9A]*$/).required(),
                    answer: joi.string().trim().required(),
                    question_id: joi.number().required()
                }
                //payload,path params
            },
        },
        handler: async function(request, h) {
            let response = voteController.create(request.payload,{});
            return response;
        }
    },
    {
        method: ["POST"], 
        path: '/vote/count/', 
        config: {
            tags: ['api'],
            auth: false , 
            validate:{
                payload:{
                    question_id: joi.number().required()
                }
                //payload,path params
            },
        },
        handler: async function(request, h) {
            let response = voteController.count(request.payload,{});
            return response;
        }
    }
]