const {to} = require('await-to-js');
const pe = require('parse-error');

module.exports.to = async (promise) => {
    let err, res;
    [err, res] = await to(promise);
    if(err) return [pe(err)];

    return [null, res];
};

module.exports.ReE = function(res, err, code=400){ // Error Web Response
    if(typeof err == 'object' && typeof err.message != 'undefined'){
        err = err.message;
    }

    let send_data = {success:false,statusCode: code, error: err};

    if(typeof res == 'object'){
        res = Object.assign(send_data, res);//merge the objects
    }

    return res;
};

module.exports.ReS = function(res, data, code=200){ // Success Web Response
    let send_data = {success:true,statusCode: code};

    if(typeof data == 'object'){
        res = Object.assign(data, send_data, res);//merge the objects
    }

    return res
};

module.exports.TE = TE = function(err_message, log){ // TE stands for Throw Error
    if(log === true){
        console.error(err_message);
    }

    throw new Error(err_message);
};