'use strict';

const Hapi = require('hapi');

const CONFIG = require('./config/config');
const {TE, to}          = require('./services/util.service');

//# swagger
//http://localhost:3000/documentation
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const SwaggerOptions = {
    info: {
        'title': 'Test API Documentation',
        'version': '0.0.1'
    }
};

const server = Hapi.server({
    port: CONFIG.port,
    routes: {cors: {origin: ['*']} }
});

const init = async () => {

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: SwaggerOptions
        }
    ]);

    try {
        await server.register({
            plugin: require('hapi-router'),
            options: {
                routes: 'routes/*.js' // uses glob to include files
            }
        })
    } catch (err) {
        // Handle err
        throw err
    }

    await server.start();

    return server;
};

//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init().then(server => {
  console.log('Server running at:', server.info.uri);
})
.catch(err => {
  console.log(err);
});

module.exports = server; // for testing