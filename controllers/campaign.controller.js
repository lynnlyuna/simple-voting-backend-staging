const { campaignModel,voteModel }  = require('../models');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){

    let [err, campaign] = await to(campaignModel.create(req));

    if(err) return ReE(res, err, 422);
    return ReS(res, {message:'Successfully created new campaign.', data:campaign}, 201);
}
module.exports.create = create;

const get = async function(req, res){

    let condi = {};

    if(req.id) condi = {question_id:req.id}

    let [err, campaign] = await to(campaignModel.findOne(condi));

    if(err) return ReE(res, err, 422);
    if(!campaign) return ReE(res, {message:"Invalid data"}, 422);

    //Check Start time
    let x = new Date(campaign.start_time);
    let y = new Date();
    let z = new Date(campaign.end_time);
    if(x > y) return ReE({data: campaign}, {message:"Not yet start"}, 201);
    else if(y > z) return ReE({data: campaign}, {message:"Voting ended"}, 201);
    return ReS(res, {data: campaign});
}
module.exports.get = get;

const get_valid = async function(req, res){

    let condi = {
        start_time : 
        {     
            $lt: new Date(new Date().setHours(00,00,00))
        },
        end_time : 
        {     
            $gte: new Date(new Date().setHours(00,00,00))
        },
    };

    let [err, campaign] = await to(campaignModel.find(condi));

    if(err) return ReE(res, err, 422);
    if(typeof campaign == 'undefined' || campaign.length == 0) return ReE(res, {message:"Invalid data"}, 422);
    return ReS(res, {data: campaign});
}
module.exports.get_valid = get_valid;

const get_list = async function(req, res){

    let results = [];

    let [err, campaign] = await to(campaignModel.find({}).sort([['end_time', -1]]));

    if(err) return ReE(res, err, 422);
    if(typeof campaign == 'undefined' || campaign.length == 0) return ReE(res, {message:"Invalid data"}, 422);

    //Get Voted Count
    const promises = []  // collect all promises here
    campaign.forEach(item => {
        const promise = voteModel.countDocuments({ question_id: item.question_id })
        promises.push(promise)
    })
    await Promise.all(promises).then(result => {
        campaign.forEach((item,i) => {
            //Check Start time
            let status = 0; //-1: Not yet start, 0: Started, 1: Voting ended
            let x = new Date(item.start_time);
            let y = new Date();
            let z = new Date(item.end_time);
            if(x > y) status = -1;
            else if(y > z) status = 1;

            results.push(Object.assign({},campaign[i].toJSON(),{voted: result[i],status:status}));
        })
    })

    //i. display campaigns within start/end time first and order by total no. of votes.
    //ii. display most recent ended campaign afterward
    results.sort((a,b) => {
        if (a.status==0 ){
            if(b.status !=0) return -1;
            if(a.voted > b.voted) return -1;
            return 1;
        }
        else if (a.status==-1) {
            if(b.status ==0) return 1;
            if(b.status ==1) return -1;

            let x = new Date(a.start_time);
            let y = new Date(b.start_time);
            if(x > y) return -1;

            return 1;
        }else{
            if(b.status !=1) return 1;
        }
        return 0;
    });

    return ReS(res, {data: results});
}
module.exports.get_list = get_list;