const { voteModel }          = require('../models');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){

    let err, vote;

    //Check duplicate hkid
    [err, vote] = await to(voteModel.find({
        hkid:req.hkid,
        question_id: req.question_id
    }));

    if(err) return ReE(res, err, 422);

    if(typeof vote !== 'undefined' && vote.length > 0) return ReE({answer:vote[0]["answer"]}, {message:"Duplicate HKID"}, 422);

    [err, vote] = await to(voteModel.create(req));

    if(err) return ReE(res, err, 422);
    return ReS(res, {message:'Successfully created new vote.', data:vote}, 200);
}
module.exports.create = create;

const get = async function(req, res){
    let err, vote;

    [err, vote] = await to(voteModel.find({
        question_id:req.id
    }));

    if(err) return ReE(res, err, 422);
    if(!vote) return ReE(res, {message:"Invalid data"}, 422);
    return ReS(res, {data: vote});
}
module.exports.get = get;

const count = async function(req, res){

    let aggregatorOpts = [
        {$match : {question_id: req.question_id}},
        {$group : {_id:{answer:"$answer",question_id:"$question_id"}, count:{$sum:1},
        entry: {
            $push: {
              answer: "$answer"
            }
        }}}
    ]

    let [err, cnt] = await to(voteModel.aggregate(aggregatorOpts));

    if(err) return ReE(res, err, 422);

    let answers = {};
    cnt.forEach(element => {
        if(element._id["answer"])
            answers[element._id["answer"]] = element.count;
    });
    return ReS(res, {data: answers});
}
module.exports.count = count;