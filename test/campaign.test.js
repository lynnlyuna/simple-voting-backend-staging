let chai = require('chai');
let expect = chai.expect;
//Server 
let server = require('../server.js');
//let loginUser = require('./login.js');
let auth = {token: ''};

describe('/GET campaign', () => {  

  beforeEach(function(done) {
    //loginUser(auth, done);
    done();
  });

  it('it should GET the first campaign', async () => {

    // these must match the route you want to test
    const injectOptions = {
      method: 'GET',
      url: '/campaign/1'
    }

    // wait for the response and the request to finish
    const response = await server.inject(injectOptions)

    //set your expectations
    expect(response.statusCode).to.equal(200);
    expect(response.result).have.property('data').have.property('answer').be.instanceof(Array).and.have.length(4);
    expect(response.result).have.property('error').equal('Voting ended');
  })

  it('it should GET the campaign list', async () => {

    // these must match the route you want to test
    const injectOptions = {
      method: 'GET',
      url: '/campaign-list/'
    }

    // wait for the response and the request to finish
    const response = await server.inject(injectOptions)

    //set your expectations
    expect(response.statusCode).to.equal(200);
    expect(response.result).have.property('data').be.instanceof(Array).and.have.lengthOf.above(0);
  })
})
