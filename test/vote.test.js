let chai = require('chai');
let expect = chai.expect;
// Interesting part
let server = require('../server.js');
//let loginUser = require('./login.js');
let auth = {token: ''};

describe('/POST VOTE', () => {  

  beforeEach(function(done) {
    //loginUser(auth, done);
    done();
  });

  it('it should get the vote count', async () => {

    // these must match the route you want to test
    const injectOptions = {
      method: 'POST',
      url: '/vote/count/',
      payload: {
        "question_id": 1
      }
    }

    // wait for the response and the request to finish
    const response = await server.inject(injectOptions)

    //set your expectations
    expect(response.statusCode).to.equal(200);
    expect(response.result).have.property('data').have.property('Kobe Bryant').to.be.a('number');
  })

  it('it should display Duplicate HKID (Add new vote)', async () => {

    // these must match the route you want to test
    const injectOptions = {
      method: 'POST',
      url: '/vote/create/',
      payload: {
        "hkid": "R2412451",
        "answer": "Kobe Bryant",
        "question_id": 1
      }
    }

    // wait for the response and the request to finish
    const response = await server.inject(injectOptions)

    //set your expectations
    expect(response.statusCode).to.equal(200);
    //expect(response.result).have.property('message').to.equal('Successfully created new vote.');
    //expect(response.result).have.property('error').to.equal('Duplicate HKID');
    expect(response.result).satisfy(function(res) {
      return res['message'] == "Successfully created new vote." || res['error'] == "Duplicate HKID";
    })
  })
})