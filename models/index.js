let mongoose = require( 'mongoose' );
//let Schema   = mongoose.Schema;

mongoose.connect( 'mongodb://vote:abcd1234@ds145694.mlab.com:45694/voting', { useNewUrlParser: true } );

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("database connected");
  /*db.modelNames()
    .forEach(name => {
      console.log(`model name ${name}`);                                                                              
  });*/
});

let campaignSchema = new mongoose.Schema({
  "question_id": Number,
  "question": String,
  "start_time": Date,
  "end_time": Date,
  "answer": [String]
});

let campaignModel = mongoose.model( 'campaign' , campaignSchema);
/*
campaignModel.create({ 
  "question_id": 2,
  "question": "2: Which HK CEO candidate you are preferred.",
  "start_time": "2019-01-01 00:00:00",
  "end_time": "2019-01-31 23:59:59",
  "answer": [
      "Carrie Lam",
      "John Tsang",
      "Rebecca Ip"
  ]
 }, function (err, docs) {
  console.log(docs); 
});*/

module.exports.campaignModel = campaignModel;

let voteSchema = new mongoose.Schema({
  "question_id": Number,
  "hkid": String,
  "datetime": {
    type: Date,
    default: Date.now
  },
  "answer": String
});
let voteModel     = mongoose.model( 'vote' , voteSchema);
module.exports.voteModel = voteModel;